src_dir: ./
output_dir: ./docs
project: Tetran
project_github: https://github.com/fortran-gaming/tetran
project_website: https://fortran-gaming.github.io/tetran
summary: object-oriented Fortran falling-block game
author: Michael Hirsch Ph.D.
author_description: SciVision, Inc.
github: https://github.com/scivision
license: by
exclude: CMakeFortranCompilerId.F
display: public
         protected
         private
source: false
graph: true
search: true
